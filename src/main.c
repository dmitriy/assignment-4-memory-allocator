#include "mem.h"
#include "mem_internals.h"

#include <stdbool.h>
#include <stdio.h>

#define HEAP_SIZE 4096
#define BLOCK_SIZE_1 64
#define BLOCK_SIZE_2 128
#define BLOCK_SIZE_3 256
#define BLOCK_SIZE_4 2048
#define BLOCK_SIZE_5 3072

void test_malloc_success()
{
    heap_init(HEAP_SIZE);

    void *ptr = _malloc(BLOCK_SIZE_1);
    struct region *region = (struct region *)ptr - 1;

    if (!region_is_invalid(region))
    {
        printf("Test malloc_success passed\n");
    }
    else
    {
        printf("Test malloc_success failed\n");
    }

    heap_term();
}

void test_free_single_block()
{
    heap_init(HEAP_SIZE);

    void *ptr1 = _malloc(BLOCK_SIZE_1);
    void *ptr2 = _malloc(BLOCK_SIZE_2);

    _free(ptr1);

    if (region_is_invalid((struct region *)ptr1 - 1) && !region_is_invalid((struct region *)ptr2 - 1))
    {
        printf("Test free_single_block passed\n");
    }
    else
    {
        printf("Test free_single_block failed\n");
    }

    heap_term();
}

void test_free_multiple_blocks()
{
    heap_init(HEAP_SIZE);

    void *ptr1 = _malloc(BLOCK_SIZE_1);
    void *ptr2 = _malloc(BLOCK_SIZE_2);
    void *ptr3 = _malloc(BLOCK_SIZE_3);

    _free(ptr1);
    _free(ptr3);

    if (region_is_invalid((struct region *)ptr1 - 1) && !region_is_invalid((struct region *)ptr2 - 1) && region_is_invalid((struct region *)ptr3 - 1))
    {
        printf("Test free_multiple_blocks passed\n");
    }
    else
    {
        printf("Test free_multiple_blocks failed\n");
    }

    heap_term();
}

void test_expand_memory_region()
{
    heap_init(HEAP_SIZE);

    void *ptr1 = _malloc(BLOCK_SIZE_4);
    void *ptr2 = _malloc(BLOCK_SIZE_4);

    _free(ptr1);

    void *ptr3 = _malloc(BLOCK_SIZE_5);

    if (!region_is_invalid((struct region *)ptr1 - 1) && !region_is_invalid((struct region *)ptr2 - 1) && !region_is_invalid((struct region *)ptr3 - 1))
    {
        printf("Test expand_memory_region passed\n");
    }
    else
    {
        printf("Test expand_memory_region failed\n");
    }

    heap_term();
}

void test_allocate_new_memory_region()
{
    heap_init(HEAP_SIZE);

    void *ptr1 = _malloc(BLOCK_SIZE_4);
    void *ptr2 = _malloc(BLOCK_SIZE_4);

    _free(ptr1);

    void *ptr3 = _malloc(BLOCK_SIZE_3);

    void *ptr4 = _malloc(BLOCK_SIZE_5);

    if (!region_is_invalid((struct region *)ptr1 - 1) && !region_is_invalid((struct region *)ptr2 - 1) &&
        region_is_invalid((struct region *)ptr3 - 1) && !region_is_invalid((struct region *)ptr4 - 1))
    {
        printf("Test allocate_new_memory_region passed\n");
    }
    else
    {
        printf("Test allocate_new_memory_region failed\n");
    }

    heap_term();
}

int main()
{
    test_malloc_success();
    test_free_single_block();
    test_free_multiple_blocks();
    test_expand_memory_region();
    test_allocate_new_memory_region();

    return 0;
}
