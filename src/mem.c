#define _DEFAULT_SOURCE

#include <unistd.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next)
{
  *((struct block_header *)addr) = (struct block_header){
      .next = next,
      .capacity = capacity_from_size(block_sz),
      .is_free = true};
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);

static void *map_pages(void const *addr, size_t length, int additional_flags)
{
  return mmap((void *)addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

/* - - - - - - */
static struct region alloc_region(const void *addr, size_t query)
{
  size_t size_for_region = query + offsetof(struct block_header, contents);
  size_t adjusted_region_size = region_actual_size(size_for_region);
  void *mapped_addr = NULL;
  int flags = MAP_FIXED | MAP_FIXED_NOREPLACE;
  mapped_addr = map_pages(addr, adjusted_region_size, flags);

  struct region new_region;
  new_region.addr = (mapped_addr != MAP_FAILED) ? mapped_addr : NULL;
  new_region.size = adjusted_region_size;
  new_region.extends = (mapped_addr != MAP_FAILED);

  if (!new_region.extends)
  {
    new_region.addr = map_pages(addr, adjusted_region_size, 0);
    if (new_region.addr == MAP_FAILED)
      return REGION_INVALID;
  }

  block_size b_size = {adjusted_region_size};
  block_init(new_region.addr, b_size, NULL);

  return new_region;
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial)
{
  const struct region region = alloc_region(HEAP_START, initial);
  if (region_is_invalid(&region))
    return NULL;

  return region.addr;
}

/* - - - - - - */
void heap_term()
{
  for (struct block_header *block = (struct block_header *)HEAP_START; block != NULL;)
  {
    struct block_header *next_block = block;
    size_t total_region_size = 0;

    do
    {
      total_region_size += size_from_capacity(next_block->capacity).bytes;
      next_block = next_block->next;
    } while (next_block && (next_block == block_after(block)));

    void *region_start = block;
    block = next_block;

    int r = munmap(region_start, total_region_size);
    if (r == -1)
    {
      printf("error in munmap");
    }
  }
}

#define BLOCK_MIN_CAPACITY 24

static bool block_splittable(struct block_header *restrict block, size_t query)
{
  return block->is_free &&
         query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

/* - - - - - - */
static bool split_if_too_big(struct block_header *block, size_t query)
{
  if (block == NULL)
  {
    return false;
  }

  size_t adjusted_query = query < BLOCK_MIN_CAPACITY ? BLOCK_MIN_CAPACITY : query;
  if (!block_splittable(block, adjusted_query))
  {
    return false;
  }

  size_t current_capacity = size_from_capacity(block->capacity).bytes;
  size_t requested_capacity = size_from_capacity((block_capacity){adjusted_query}).bytes;

  if (current_capacity <= requested_capacity)
  {
    return false;
  }

  size_t remaining_capacity = current_capacity - requested_capacity;
  void *next_block = (uint8_t *)block + requested_capacity;

  block_size new_block_size = {remaining_capacity};
  block_size original_block_size = {requested_capacity};

  block_init(next_block, new_block_size, block->next);
  block_init(block, original_block_size, next_block);

  return true;
}

static void *block_after(struct block_header const *block)
{
  return (void *)(block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
    struct block_header const *fst,
    struct block_header const *snd)
{
  return (void *)snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd)
{
  return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

/* - - - - - - */
static bool try_merge_with_next(struct block_header *block)
{
  struct block_header *next_block;

  if (!(block && (next_block = block->next) && mergeable(block, next_block)))
  {
    return false;
  }

  size_t additional_capacity = size_from_capacity(next_block->capacity).bytes;
  block->capacity.bytes += additional_capacity;

  block->next = next_block->next;

  return true;
}

struct block_search_result
{
  enum
  {
    BSR_FOUND_GOOD_BLOCK,
    BSR_REACHED_END_NOT_FOUND,
    BSR_CORRUPTED,
    BSR_QUERY_TOO_SMALL
  } type;
  struct block_header *block;
};

/* - - - - - - */
static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz)
{
  struct block_header *res_block = block;

  do
  {
    if (block != NULL)
    {

      if (try_merge_with_next(block))
        continue;
      if (block->is_free && block->capacity.bytes >= sz)
        return (struct block_search_result){.block = block, .type = BSR_FOUND_GOOD_BLOCK};
      if (block->next == NULL)
        res_block = block;
      block = block->next;
    }
    else
    {
      return (struct block_search_result){.block = res_block, .type = BSR_REACHED_END_NOT_FOUND};
    }
  } while (1);
}

/* - - - - - - */
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block)
{
  struct block_search_result result;
  result = find_good_or_last(block, query);

  if (query < BLOCK_MIN_CAPACITY)
  {
    result.type = BSR_QUERY_TOO_SMALL;
    return result;
  }

  bool isGoodBlock = (result.type == BSR_FOUND_GOOD_BLOCK);
  if (isGoodBlock)
  {
    if (split_if_too_big(result.block, query))
    {
      result.block->is_free = false;
    }
  }

  return result;
}

/* - - - - - - */

static struct block_header *grow_heap(struct block_header *restrict last,
                                      size_t query)
{
  if (!last)
  {
    return NULL;
  }

  void *end_of_last = block_after(last);
  struct region new_reg = alloc_region(end_of_last, query);

  if (region_is_invalid(&new_reg))
  {
    return NULL;
  }

  last->next = new_reg.addr;
  try_merge_with_next(last);

  if (last->next)
  {
    return last->next;
  }
  else
  {
    return last;
  }
}

/* - - - - - - */
static struct block_header *memalloc(size_t query, struct block_header *heap_start)
{
  if (!heap_start)
    return NULL;

  size_t adjusted_query = (query < BLOCK_MIN_CAPACITY) ? BLOCK_MIN_CAPACITY : query;
  struct block_search_result search_result = try_memalloc_existing(adjusted_query, heap_start);

  switch (search_result.type)
  {
  case BSR_FOUND_GOOD_BLOCK:
    return search_result.block;

  case BSR_CORRUPTED:
    return NULL;

  default:
    break;
  }

  struct block_header *extended_region = grow_heap(search_result.block, adjusted_query);
  return extended_region ? try_memalloc_existing(adjusted_query, heap_start).block : NULL;
}

void *_malloc(size_t query)
{
  struct block_header *const addr = memalloc(query, (struct block_header *)HEAP_START);
  if (addr)
    return addr->contents;
  return NULL;
}

static struct block_header *block_get_header(void *contents)
{
  return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}

/* - - - - - - */
void _free(void *mem)
{
  if (mem == NULL)
  {
    return;
  }

  struct block_header *header = block_get_header(mem);
  header->is_free = true;

  for (; try_merge_with_next(header);)
  {
  }
}
